//
//  CalcState.swift
//  Calculator
//
//  Created by Alan on 12/28/15.
//  Copyright © 2015 Alan. All rights reserved.
//

import Foundation

class CalcState {
    
    //enum for the calculator operations
    enum CalcOperation {
        case ADD
        case SUBTRACT
        case MULTIPLY
        case DIVIDE
        case MODULO
        case EQUAL
        case POWER
    }
    
    //class variables
    var isTotalCleared: Bool = true
    var lastValue: Double = 0.0
    var currentValue: Double = 0.0
    var lastOperation: CalcOperation = CalcOperation.EQUAL
    var memory : Double = 0.0
    var isDecimal: Bool = false
    var clearDisplay: Bool = false
    
}
