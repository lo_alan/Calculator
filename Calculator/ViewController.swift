//
//  ViewController.swift
//  Calculator
//
//  Created by Alan on 3/25/15.
//  Copyright (c) 2015 Alan. All rights reserved.
//

import UIKit
import Swift

class ViewController: UIViewController{
    //enum for the calculator operations
    enum CalcOperation {
        case ADD
        case SUBTRACT
        case MULTIPLY
        case DIVIDE
        case MODULO
        case EQUAL
        case POWER
    }
    
    @IBOutlet weak var display: UILabel!
    //function for displaying the current number value
    func displayValue() {
        if currentValue.isInfinite {
            display.text = "INF"
            print("currentValue : INF")
        }
        else if currentValue.isNaN {
            display.text = "NAN"
            print("current value : NaN")
        }
        else if currentValue == round(currentValue) {
            let decimal = NSDecimalNumber(double: currentValue)
            display.text = String(decimal)
        }
        else {
            display.text = String(currentValue)
        }
        print("displayed value \(display.text)")
    }
    
    //class variables
    var isTotalCleared: Bool = true
    var lastValue: Double = 0.0
    var currentValue: Double = 0.0
    var lastOperation: CalcOperation = CalcOperation.EQUAL
    var memory : Double = 0.0
    var isDecimal: Bool = false
    var clearDisplay: Bool = false
    
    //funciton for adding a digit to the end of the display
    
    @IBAction func appendDigit(sender: UIButton) {
        let digit = sender.currentTitle!
        if clearDisplay {
            display.text = ""
            currentValue = 0.0
            isDecimal = false
            clearDisplay = false
        }
        if !isTotalCleared {
            display.text = display.text! + digit
        } else {
            display.text = digit
            isTotalCleared = false
        }
        currentValue = Double(display.text!)!
        print("digit added - \(digit)")
    }
    
    //function for taking a digit away from the end of the display
    
    @IBAction func removeDigit(sender: UIButton) {
        let str = display.text
        display.text = String(str!.characters.dropLast(1))
        if (display.text!.characters.count) == 0 {
            currentValue = 0.0
            clearDisplay = true
        }
        else if (display.text!.characters.count) == 1 &&
            currentValue < 0.0 {
            currentValue = 0.0
            clearDisplay = true
        }
        else if Double(display.text!)! !=  0.0 {
            currentValue = Double(display.text!)!
        }
        else {
            currentValue = 0
            display.text = "0"
        }
        displayValue()
        print("removed digit \(display.text)")
    }
    
    //function for clearing the display
    
    @IBAction func clear(sender: UIButton) {
        isTotalCleared = true
        lastValue = 0.0
        currentValue = 0.0
        lastOperation = CalcOperation.EQUAL
        memory = 0.0
        isDecimal = false
        clearDisplay = false
        display.text = "0"
        print("cleared")
    }
    
    //function for putting a decimal (only using the button, not division)
    
    @IBAction func decimal(sender: UIButton) {
        if clearDisplay {
            display.text = "0"
            currentValue = 0.0
            isDecimal = false
            clearDisplay = false
        }
        if !isDecimal {
            display.text = display.text! + "."
            isDecimal = true
            isTotalCleared = false
            print("decimal added to \(currentValue)")
        }
    }
    
    //function for adding values
    
    @IBAction func add(sender: UIButton) {
        lastValue = Double(display.text!)!
        lastOperation = CalcOperation.ADD
        clearDisplay = true
        print("set last operation to add")
    }
    
    //function for subtracting values
    
    @IBAction func subtract(sender: UIButton) {
        lastValue = Double(display.text!)!
        lastOperation = CalcOperation.SUBTRACT
        clearDisplay = true
        print("set last operation to sutract")
    }
    
    //function for multiplying values
    
    @IBAction func multiply(sender: UIButton) {
        lastValue = Double(display.text!)!
        lastOperation = CalcOperation.MULTIPLY
        clearDisplay = true
        print("set last operation to multiply")
    }
    
    //function for dividing values (only other way to get a decimal number without the button)
    
    @IBAction func divide(sender: UIButton) {
        lastValue = Double(display.text!)!
        lastOperation = CalcOperation.DIVIDE
        clearDisplay = true
        print("set last operation to divide")
    }
    
    //function for getting the remainder of values when divided (modulo or modulus)
    
    @IBAction func modulo(sender: UIButton){
        lastValue = Double(display.text!)!
        lastOperation = CalcOperation.MODULO
        clearDisplay = true
        print("set last operation to modolo")
    }
    
    //function for putting/taking a - symbol (nevgetive or positive)
    
    @IBAction func sign(sender: UIButton){
        //instant function, will change value on the click
        currentValue *= -1
        displayValue()
        print("fliped number to positive/negetive")
    }
    
    //memory functions (to find the type of function, look at the name)
    
    @IBAction func memoryRecover(sender: UIButton){
        if memory == round(memory) {
            display.text = String(Int(memory))
        } else {
            display.text = String(memory)
        }
        print("recalled memorized value")
    }
    
    @IBAction func memoryClear(sender: UIButton){
        memory = 0.0
        print("memorized value cleared")
    }
    
    @IBAction func memoryAdd(sender: UIButton){
        memory = currentValue
        print("memorizing value")
    }
    
    //powering numbers function
    
    @IBAction func power(sender: UIButton){
        lastValue = Double(display.text!)!
        lastOperation = CalcOperation.POWER
        clearDisplay = true
        print("set last operation to POW")
    }
    
    @IBAction func squareRoot(sender: UIButton){
        //instant function, will change value on the click
        currentValue = sqrt(currentValue)
        displayValue()
        print("given square root of current number")
    }
    
    //function for completing changing in values (using the = button)
    @IBAction func equal(sender: UIButton) {
        if currentValue != Double.infinity {
            switch lastOperation {
            case CalcOperation.ADD:
                currentValue = lastValue + currentValue
            case CalcOperation.SUBTRACT:
                currentValue = lastValue - currentValue
            case CalcOperation.MULTIPLY:
                currentValue = lastValue * currentValue
            case CalcOperation.DIVIDE:
                if currentValue != 0 {
                    currentValue = lastValue / currentValue
                } else {
                    currentValue = Double.infinity
                    print("divided by 0 is INF")
                }
            case CalcOperation.MODULO:
                currentValue = lastValue % currentValue
            case CalcOperation.POWER:
                currentValue = pow(lastValue, currentValue)
            default:
                break
            }
            displayValue()
            clearDisplay = true
            print("changed values (using equals button)")
        }
    }
}
